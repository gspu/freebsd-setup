#!/bin/sh

set -e

pkg install -y \
  isync \
  w3m

cd /usr/ports/mail/mu

make install
cp -R work/mu-1.2/mu4e /usr/local/share/emacs/site-lisp
