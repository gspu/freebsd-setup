#!/bin/sh

set -e

pkg install -y \
    aspell \
    aspell-ispell \
    en-aspell
