#!/bin/sh

set -e

git clone https://github.com/rbenv/rbenv.git ~/.rbenv

export PATH="$HOME/.rbenv/bin:$PATH"
export RUBY_CONFIGURE_OPTS=--disable-dtrace

eval "$(rbenv init -)"

git clone https://github.com/rbenv/ruby-build.git "$(rbenv root)"/plugins/ruby-build

rbenv install 2.5.1
rbenv global 2.5.1
